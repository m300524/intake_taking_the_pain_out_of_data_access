# Intake Taking The Pain Out Of Data Access

DKRZ Tech Talk: "Intake - Taking the pain out of data access" [Video](https://www.youtube.com/watch?v=urL17kRUinE) happened 29.09.2020

[Event Description](https://indico.dkrz.de/event/31/)

Software used:

- [intake](https://intake.readthedocs.io/en/stable/)
- [intake-xarray](https://intake-xarray.readthedocs.io/en/stable/)
- [intake-esm](https://intake-esm.readthedocs.io/en/stable/)
